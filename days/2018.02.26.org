#+STARTUP: showall
#+TITLE: 30 days of Nix. Day 11
#+DATE: [2018-02-26 Mon]


** Added pulse audio

   Added pulseaudio to nixos config. This fixed the problem of firefox not able to
   play audio.

   I added the following line to my nixos config 

   #+BEGIN_SRC nix 
     hardware.pulseaudio.enable = true;
   #+END_SRC

