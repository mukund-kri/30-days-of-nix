#+STARTUP: showall
#+TITLE: 30 days of Nix. Day 2
#+DATE: [2018-02-15 Thu]


** Replacing docker-compose with nix for development

   This is early days but ... the way to use nix for development of a 
   certian environment is ...

   1. Create a default.nix in the root of the project with all the nix 
      definitions. This will contain the nix code that will define the 
      packages, env etc.

   2. Use nix-shell to enter into that environment.


** Tasks for the next few days

*** Nix'ify my snippets
*** Read through more on nix tools
*** Read more on nixpkgs

