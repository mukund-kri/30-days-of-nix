#+STARTUP: showall
#+TITLE: 30 days of Nix. Day 8
#+DATE: [2018-02-21 Wed]


** Retrospective
   More than a week into this challenge and I am just now able to write nix 
   expressions that I fully understand when I copy and paste from the web. That 
   being said many concepts are still beyond me.

   1. shell.nix vs. default.nix when using nix-shell 
   2. The nix language. man its like nothing I have ever seen before
   3. I'm just getting the hang of purity wrt. unix packages
   4. And then there is NixOS config, which though written in the nix language it
      is a different beast altogether.

** Next steps
   I need really READ the Nix Pills website
   https://nixos.org/nixos/nix-pills/index.html
   
   I think I should devote some time each day to a chapter of Nix pills as part of 
   the 30 days of nix challenge, starting tomorrow.

   Python and Ruby environments. I really need to understand these two environments
   from the nix perspective. Because I have a bunch of projects in the pipeline
   that are python/ruby based.

   NixOS config. I really need to carve out some time to organize my nixos config
   so that it is usable across all my computers.

** Thoughts
   All in all its been a productive 7 days with respect to nix and hope the 
   the rest of challenge goes just as well.


** Nixos config
   I finally rewrote the nixos config for my laptop and put it on bitbucket
   here ...

   https://bitbucket.org/mukund_kri/my-nixos-config

